# Jnktn's Monitoring scripts

These scripts are designed to run as cron job, test various parts of our infrastructure and report the issues to jnktn.tv Tech Matrix room.

## Currently implemented tests

- website update checker: reports on Thursday if website is not updated during the week

## License

© 2022 Jnktn.tv distributed under the terms of [GPL-3.0+ license](LICENSE).

See [git commits history](https://codeberg.org/jnktn/monitoring/commits/branch/main) for the complete list of collaborators.