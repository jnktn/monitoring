from datetime import *

import requests
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
from dateutil.tz import *

server_response = requests.get("https://www.jnktn.tv")
last_modified = server_response.headers["Last-Modified"]
notification_lead_time = 2

print("Server last updated (string):", last_modified)

date_last_modified = parse(last_modified)
print("Server last updated (parsed):", date_last_modified)

date_sunday = parse("Sunday 00:00 UTC")  # returns either today or the next sunday
date_now = datetime.now(tzoffset(None, 0))
print("Date-Time Now:", date_now)

date_last_sunday = date_sunday + relativedelta(days=-7)

print("Last Sunday:", date_last_sunday)

date_diff = relativedelta(date_last_modified, date_last_sunday)

modified_after_last_sunday = date_diff.days >= 0

print("Website is updated after last Saturday:", modified_after_last_sunday)
if not modified_after_last_sunday:
    time_left_to_next_sunday = date_sunday - date_now
    print("Days left till the next Saturday:", time_left_to_next_sunday.days)
    if time_left_to_next_sunday.days <= notification_lead_time:
        error_message = "Less than {} days are left to Saturday and the website is not updated yet!".format(
            notification_lead_time
        )
        print(error_message)
        with open(".report", "a+") as reports_file:
            reports_file.write("@room: " + error_message + "\n")
        exit(1)
