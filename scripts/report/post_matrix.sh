#!/bin/sh

_reports_file=".report"

_color_red="\033[31;49;1m"
_color_green="\033[32;49;1m"
_color_reset="\033[0m"


if [ ! -f "${_reports_file}" ]; then
	echo "${_color_green}File '${_reports_file}' could not be found! No message will be sent to Matrix.${_color_reset}"
	exit 0;
else
    echo "Contents of ${_reports_file}:"
    command cat ${_reports_file}
fi


command matrix-commander --login password --user-login "$MATRIX_USERNAME" --password "$MATRIX_PASSWORD" --homeserver "$MATRIX_HOMESERVER" --device "Codeberg CI" --room-default "$MATRIX_ROOM" --logout me --split "\n" -m "$(cat "${_reports_file}")"